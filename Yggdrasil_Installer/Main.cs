﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Yggdrasil_Installer
{
    public partial class Main : Form
    {
        public static string status = "Downloading...";
        public static string sub = "Downloading main installer...";

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
        );

        public Main()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            try
            {
                WebClient webClient = new WebClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                webClient.DownloadProgressChanged += (s, ee) =>
                {
                    label2.Text = ee.ProgressPercentage + "%";
                };
                webClient.DownloadFileCompleted += (s, ee) =>
                {
                    try
                    {
                        label2.Text = "Installing...";
                        var process = Process.Start(Path.Combine(Path.GetTempPath(), "ygg_setup.exe"), "/verysilent");
                        process.EnableRaisingEvents = true;
                        process.Exited += (sender, e) =>
                        {
                            this.Invoke((MethodInvoker)delegate
                            {
                                Directory.SetCurrentDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Yggdrasil"));
                            });
                            DialogResult dialogresult = MessageBox.Show("Download and install complete. First run to check for updates?", "Yggdrasil Installer", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (dialogresult == DialogResult.Yes)
                            {
                                Process.Start(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Yggdrasil", "Launcher.exe"));
                            }
                            Environment.Exit(0);
                        };
                    } catch (Exception e)
                    {
                        MessageBox.Show("Oh no, something happened...\n\n" + e, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Environment.Exit(0);
                    }
                };
                webClient.DownloadFileAsync(new Uri("https://updates.koyu.space/yggdrasil/ygg_setup.exe"), Path.Combine(Path.GetTempPath(), "ygg_setup.exe"));
            } catch
            {

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
